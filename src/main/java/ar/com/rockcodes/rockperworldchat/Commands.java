package ar.com.rockcodes.rockperworldchat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Commands implements CommandExecutor{
	@Override
	public boolean onCommand(CommandSender sender, Command command,String label, String[] args) {
		if(sender instanceof Player){
			Player player = (Player)sender;
			if(!player.isOp()){
				player.sendMessage(ChatColor.RED+"You don't have permissions.");
				return true;
			}
			
		}
		
		PerWorldChat.plugin.reloadConfig();
		PerWorldChat.plugin.config = PerWorldChat.plugin.getConfig();
		sender.sendMessage(ChatColor.GREEN+"Config reloaded");
		return true;
	}
}
