package ar.com.rockcodes.rockperworldchat;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PerWorldChat extends JavaPlugin implements Listener {
    
	public FileConfiguration config;
	public static PerWorldChat plugin;
	
    @Override
    public void onEnable() {
    		PerWorldChat.plugin = this;
            this.saveDefaultConfig();
			if (!VaultHelper.setupPermissions()) {
			    getLogger().severe("Cannot link with Vault for permissions! Disabling plugin!");
			    getServer().getPluginManager().disablePlugin(this);
			    return;
			}
            Bukkit.getServer().getPluginManager().registerEvents(this, this);
            this.getCommand("perworldchat").setExecutor(new Commands());
            this.config = this.getConfig();
    }
   
    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
    	
	    Player sender = e.getPlayer();
	    if(VaultHelper.checkPerm(sender, "perworldchat.sendallchat")) return;
	    Set<Player> r = e.getRecipients();

	    List<String> worldgroup = null;
	    
    	for(String key :this.config.getKeys(false)){
    		List<String> wg = (List<String>) this.config.getList(key);
    		if(wg.contains(sender.getWorld().getName())){
    			worldgroup = wg;
    			break;
    		}
    	}

	    for (Player pls : Bukkit.getServer().getOnlinePlayers()) {
	    		if(worldgroup!=null){
	    			if(!worldgroup.contains(pls.getWorld().getName())){
	    				if(!VaultHelper.checkPerm(pls, "perworldchat.reciveallchat") )
	    				r.remove(pls);
	    				
	    			}
	    		}else{
		            if (!pls.getWorld().getName().equals(sender.getWorld().getName())) {
		            	if(!VaultHelper.checkPerm(pls, "perworldchat.reciveallchat"))
	                    r.remove(pls);
		            	
		            }
	    		}
	    }
	    
    }
    
}